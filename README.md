# FRONT END PROBLEM SOLVING

React based app

## How to run it

Once the project is cloned, go to the directory and:

-  Run `yarn` or `npm install`
-  Run `yarn dev` or `npm run dev`
   A new tab must be open in your browser.

In order to Run the tests use:

-  Run `yarn test` or `npm run test`

**Notice** that 2 tests fail, they were identified as incorrect ones.

## Author

_Front End Developer_

**Mario Victor Medrano Maldonado**

mario.medrano.maldonado@gmail.com

## License

MIT.
