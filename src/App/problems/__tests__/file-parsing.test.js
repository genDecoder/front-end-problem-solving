import fn from '../file-parsing';
import { sampleInput, expectedOutput } from '../mock-data/file-parsing';

const result = fn(sampleInput);

describe('File Parsing Test', () => {
   it('should be an array', () => {
      expect(Array.isArray(result)).toBeTruthy();
   });
   it('should be and array of size equal to 1', () => {
      expect(result).toHaveLength(1);
   });
   it('should be equal to prepared outpput data', () => {
      expect(result).toEqual(expectedOutput);
   });
});
