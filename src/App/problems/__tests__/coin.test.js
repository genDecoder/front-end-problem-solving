import fn from '../coin';

const expectedResult = [2, 0.2, 0.1, 0.02, 0.02];

describe('Coin Problem Test', () => {
   it('should be an array and match the expected result', () => {
      const result = fn(2.34);
      expect(Array.isArray(result)).toBeTruthy();
      expect(result).toEqual(expectedResult);
   });
});
