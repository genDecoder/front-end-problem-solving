import fn from '../js-async';

describe('JS async Test', () => {
   it('should contain a user with a NAME and ROLES properties', async () => {
      const user = await fn();
      expect(user).toHaveProperty('name');
      expect(user).toHaveProperty('roles');
   });
   it('should have roles names instead of ids', async () => {
      const user = await fn();
      expect(user.roles[0]).toBe('editor');
      expect(user.roles[1]).toBe('master');
   });
});
