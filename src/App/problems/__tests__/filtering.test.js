import filter from '../filtering';
import { userList } from '../mock-data/filtering';

describe('Filter Test', () => {
   describe('DO NOT EDIT TESTS: general filtering', () => {
      it('should show all matches when searches nothing', () => {
         const res = filter(userList, ['']);
         expect(res).toHaveLength(11);
      });
      it('should show no matches no match', () => {
         const res = filter(userList, ['asdasdasd']);
         expect(res).toHaveLength(0);
      });
      it('should find the correct match', () => {
         const res = filter(userList, ['frey']);
         expect(res).toHaveLength(1);
      });
      it('should find the correct match', () => {
         const res = filter(userList, ['arry']);
         expect(res).toHaveLength(2);
      });
      it('should find the correct match', () => {
         const res = filter(userList, ['Hans-Peter']);
         expect(res).toHaveLength(1);
      });
      it('should find the correct match', () => {
         const res = filter(userList, ['Hans']);
         expect(res).toHaveLength(3);
      });
      it('should find the correct match', () => {
         const res = filter(userList, ['peter']);
         expect(res).toHaveLength(3);
      });
      it('should not be case sensitive ', () => {
         const res = filter(userList, ['Frey']);
         expect(res).toHaveLength(1);
      });
      it('should not be case sensitive ', () => {
         const res = filter(userList, ['FreY']);
         expect(res).toHaveLength(1);
      });
      it('should not be case sensitive ', () => {
         const res = filter(userList, ['A-1']);
         expect(res).toHaveLength(2);
      });
   });

   describe('TASK: general sorting of results', () => {
      it('should sort the direct matches on top', () => {
         const res = filter(userList, ['basti']);
         expect(res).toHaveLength(3);
         expect(res[0].arrayId).toEqual(7);
      });

      it('should sort the direct matches on top', () => {
         const res = filter(userList, ['a-1']);
         expect(res[0].arrayId).toEqual(8);
         // Confirmed, the correct size is 2 not 3, wrong test.
         expect(res).toHaveLength(3);
      });

      it('should sort the direct matches on top', () => {
         const res = filter(userList, ['hans']);
         expect(res).toHaveLength(3);
         expect(res[0].arrayId).toEqual(3);
         // These following 2 tests are wrong, arrayId can not be 3, 9 and 1 at the same time.
         expect(res[0].arrayId).toEqual(9);
         expect(res[0].arrayId).toEqual(1);
      });
   });

   describe('Optional Task: general sorting of results', () => {
      it('should sort the direct matches on top nearest results after', () => {
         const res = filter(userList, ['basti']);
         expect(res).toHaveLength(3);
         expect(res[0].arrayId).toEqual(7);
         expect(res[1].arrayId).toEqual(8);
         expect(res[2].arrayId).toEqual(6);
      });
   });
});
