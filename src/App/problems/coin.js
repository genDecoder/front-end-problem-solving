export default val => {
   let i = 0;
   const result = [];
   while (val > 0) {
      const coin = [2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01][i];
      const newVal = Number((val - coin).toFixed(2));
      if (newVal >= 0) {
         val = newVal;
         result.push(coin);
      } else {
         i += 1;
      }
   }
   return result;
};
