export default function(data) {
   const result = [];
   data.forEach(({ BeaconId, dbmAnt, timestamp }) => {
      const key = `${BeaconId}, ${timestamp}`;
      const existing = result.find(item => item.beacon === key);
      dbmAnt = dbmAnt || -135;

      if (existing !== undefined) {
         existing.vector.push(dbmAnt);
      } else {
         result.push({
            beacon: key,
            vector: [dbmAnt]
         });
      }
   });
   return result;
}
