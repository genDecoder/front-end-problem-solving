export const sampleInput = [
   { BeaconId: 111, antId: 1, dbmAnt: -85.8333, timestamp: '2016-10-26 00:11:00' },
   { BeaconId: 111, antId: 2, dbmAnt: -40.111, timestamp: '2016-10-26 00:11:00' },
   { BeaconId: 111, antId: 3, dbmAnt: -20.023, timestamp: '2016-10-26 00:11:00' }
];

export const expectedOutput = [
   {
      beacon: '111, 2016-10-26 00:11:00',
      vector: [
         -85.8333, // dbm for antId: 1
         -40.111, // dbm for antId: 2
         -20.023 // dbm for antId: 3
      ]
   }
];
