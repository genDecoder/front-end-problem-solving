import { dbFakeRoleRequest, dbFakeUserRequest } from './api/js-async';

export default async function() {
   const user = await dbFakeUserRequest(1);
   const arrayOfPromises = user.roles.map(async roleId => {
      const role = await dbFakeRoleRequest(roleId);
      return role.name;
   });
   user.roles = await Promise.all(arrayOfPromises);
   return user;
}
