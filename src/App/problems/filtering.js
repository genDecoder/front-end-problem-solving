export default function(list, wordList) {
   wordList = wordList.map(normalizeWord).filter(word => !isEmpty(word));
   if (wordList.length > 0) {
      list = list.filter(item => !isEmpty(getMatchValue(getItemValues(item), wordList)));
      list = doSortByMatchValue(list, wordList);
      list = doSortByDirectMatchToTop(list, wordList);
   }
   return list;
}

const isEmpty = word => word === '' || word === null;
const normalizeWord = word => word.trim().toLowerCase();
const getItemValues = ({ companyId, prename, lastname, tags }) => [
   ...tags.idrows,
   companyId,
   prename,
   lastname
];

const getMatchValue = (itemValues, wordList, exact) => {
   let matchValue = null;
   let index = itemValues.length;
   while (isEmpty(matchValue) && index) {
      index -= 1;
      const value = normalizeWord(itemValues[index]);
      if (
         wordList.filter(word => (exact ? value === word : value.indexOf(word) !== -1)).length > 0
      ) {
         matchValue = value;
      }
   }
   return matchValue;
};

const doSortByMatchValue = (list, wordList) =>
   list.sort((a, b) => {
      const aMatchValue = getMatchValue(getItemValues(a), wordList);
      const bMatchValue = getMatchValue(getItemValues(b), wordList);
      if (aMatchValue < bMatchValue) {
         return -1;
      }
      if (bMatchValue < aMatchValue) {
         return 1;
      }
      return 0;
   });

const doSortByDirectMatchToTop = (list, wordList) =>
   list.sort((a, b) => {
      const aMatchValue = getMatchValue(getItemValues(a), wordList, true);
      const bMatchValue = getMatchValue(getItemValues(b), wordList, true);
      if (aMatchValue) {
         return -1;
      }
      if (bMatchValue) {
         return 1;
      }
      return 0;
   });
