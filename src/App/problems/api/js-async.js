/*
 * just an example library that you could use for testing purposes your task is to write
 * the implementation as described in the README.md
 */

const rejectError = new Error('Something went wrong');

export function dbFakeUserRequest(id, callback) {
   const user = { name: 'Somebody', roles: [1, 2] };
   const error = new Error('Id is not a finite number');
   const isValidRequest = Number.isFinite(id);
   if (typeof callback === 'function') {
      return setImmediate(() => callback.apply(null, isValidRequest ? [null, user] : [error]));
   }
   return new Promise((resolve, reject) =>
      setImmediate(() => (isValidRequest ? resolve(user) : reject(rejectError)))
   );
}

export function dbFakeRoleRequest(id, callback) {
   const role = {};
   let isValidRequest = true;
   const error = new Error('No such Role');
   switch (id) {
      case 1:
         role.name = 'editor';
         break;
      case 2:
         role.name = 'master';
         break;
      default:
         isValidRequest = false;
   }
   if (typeof callback === 'function') {
      return setImmediate(() => callback.apply(null, isValidRequest ? [null, role] : [error]));
   }
   return new Promise((resolve, reject) =>
      setImmediate(() => (isValidRequest ? resolve(role) : reject(rejectError)))
   );
}
