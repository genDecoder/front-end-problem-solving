import { css, StyleSheet } from 'aphrodite';
import React, { Fragment } from 'react';

const TEXTS = {
   title: 'Duplicate Entries',
   instruction_p_1:
      'In a former project we had a contact form. In our production database we have found multiple entries of the same user.',
   instruction_p_2:
      'Please describe shortly your approach in debugging this situation and name 2-3 different possible reasons for this kind of "bug". For each option describe how we could fix this behaviour.',
   debugging_title: 'Debugging',
   debugging_p_1:
      'Put a break point in the function that is triggered once the button is clicked in order to see the data do we have in the UI before sending it.',
   debugging_p_2:
      'Fill the form and send it. Then check the network activity, find the specific http request and finally see what was exactly send to the server.',
   debugging_p_3:
      'Check new entries in the database (filtered by some email), or call some service named something like "getUserEntries(email);" using postman for example and check the list it returns.',
   causes_title: 'Possible Causes',
   cause_1:
      'The email "test.mario@gmail.com" is not the same as "testmario@gmail.com", but usually email providers consider both the same.',
   cause_2: 'There is no limitations for new entries:',
   solution_1_0: 'The Backend should validate and consider both formats as equal.',
   solution_1_1: 'The Frontend should transform one format into the other',
   solution_2_0: 'Receive a user entry only if the last one was answered',
   solution: 'Solution',
   and_or: 'And/Or'
};

export function DuplicateEntriesProblem() {
   return (
      <Fragment>
         <div className={css(Styles.questionContainer)}>
            <span className={css(Styles.titleText)}>{TEXTS.title}</span>
            <span className={css(Styles.paragraphText)}>{TEXTS.instruction_p_1}</span>
            <span className={css(Styles.paragraphText)}>{TEXTS.instruction_p_2}</span>
         </div>

         <div className={css(Styles.answerContainer)}>
            <span className={css(Styles.subTitleText)}>{TEXTS.debugging_title}</span>
            <span className={css(Styles.paragraphText)}>
               <span className={css(Styles.numberText)}>1.</span>
               {TEXTS.debugging_p_1}
            </span>
            <span className={css(Styles.paragraphText)}>
               <span className={css(Styles.numberText)}>2.</span>
               {TEXTS.debugging_p_2}
            </span>
            <span className={css(Styles.paragraphText)}>
               <span className={css(Styles.numberText)}>3.</span>
               {TEXTS.debugging_p_3}
            </span>
            <span className={css(Styles.subTitleText)}>{TEXTS.causes_title}</span>
            <span className={css(Styles.paragraphText)}>
               <span className={css(Styles.numberText)}>1.</span>
               {TEXTS.cause_1}
            </span>
            <span className={css(Styles.labelText)}>{TEXTS.solution}</span>
            <span className={css(Styles.quoteText)}>
               <span className={css(Styles.numberText)}>-</span>
               {TEXTS.solution_1_0}
            </span>
            <span className={css(Styles.quoteText)}>
               <span className={css(Styles.numberText)}>-</span>
               {TEXTS.solution_1_1}
            </span>
            <span className={css(Styles.paragraphText)}>
               <span className={css(Styles.numberText)}>2.</span>
               {TEXTS.cause_2}
            </span>
            <span className={css(Styles.labelText)}>{TEXTS.solution}</span>
            <span className={css(Styles.quoteText)}>
               <span className={css(Styles.numberText)}>-</span>
               {TEXTS.solution_2_0}
            </span>
         </div>
      </Fragment>
   );
}

const Styles = StyleSheet.create({
   questionContainer: {
      backgroundColor: '#ebd8d557',
      display: 'flex',
      flexDirection: 'column',
      marginBottom: 10,
      padding: 10
   },
   answerContainer: {
      backgroundColor: '#d5ebdf57',
      display: 'flex',
      flexDirection: 'column',
      marginBottom: 10,
      padding: 10
   },
   titleText: {
      fontFamily: 'Open Sans',
      fontSize: '1em',
      fontWeight: 'bold',
      marginTop: 5
   },
   subTitleText: {
      fontFamily: 'Open Sans',
      fontSize: '0.8em',
      fontWeight: 'bold',
      marginTop: 10
   },
   paragraphText: {
      fontFamily: 'Roboto',
      fontSize: '0.7em',
      marginLeft: 20,
      marginTop: 5
   },
   quoteText: {
      fontFamily: 'Roboto',
      fontSize: '0.7em',
      fontStyle: 'italic',
      marginLeft: 40,
      marginTop: 10
   },
   numberText: {
      fontFamily: 'Roboto',
      fontSize: '0.7em',
      fontWeight: 'bold',
      marginRight: 10
   },
   labelText: {
      fontFamily: 'Roboto',
      fontSize: '0.6em',
      fontWeight: 'bold',
      marginLeft: 30,
      marginTop: 10
   }
});
