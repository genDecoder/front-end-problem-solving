import { css, StyleSheet } from 'aphrodite';
import React, { useState } from 'react';

const fakeText =
   'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ornare massa eu porttitor tempor. Quisque lobortis, lectus sit amet mollis convallis, tortor metus facilisis nibh, eget egestas eros ex non dui. Curabitur at metus at nunc mattis lacinia at nec felis. Cras commodo odio a tempus tristique. Nullam et leo at libero accumsan luctus ac quis dolor. Sed eget ligula volutpat, laoreet lacus sit amet, vestibulum ex. Phasellus luctus metus quis urna interdum interdum. Praesent iaculis felis in neque dictum, ac placerat ligula elementum. Duis finibus urna vel mollis varius. Morbi non vulputate turpis. Fusce ut ante tincidunt, placerat purus vitae, vehicula leo. Pellentesque venenatis ante vitae tellus fermentum, quis mollis sapien posuere. Cras ultricies auctor urna ut porta. Suspendisse convallis nunc vel lacus pretium dictum. Phasellus pharetra ipsum felis, quis semper mi tempor quis. Nulla bibendum nisl et turpis tincidunt hendrerit. Nam cursus arcu non velit rutrum gravida. Suspendisse potenti. Sed eget pharetra metus. Sed pellentesque dolor lorem, nec hendrerit nibh mattis eu. Vivamus non scelerisque ex. Mauris condimentum semper urna quis auctor. Aliquam erat volutpat. Sed condimentum aliquam nunc, id rutrum tellus hendrerit at. Quisque ullamcorper nisi ac diam auctor, a malesuada nisl vehicula. Integer sed eros congue, fringilla augue vel, porta lacus. Duis ac eleifend metus, non interdum erat. Nam lacus tortor, faucibus sed lectus quis, pulvinar semper quam. Sed id leo ut sapien malesuada dapibus. Suspendisse eu interdum odio. Curabitur et metus et orci malesuada varius. Sed porttitor, nisl sed placerat iaculis, sapien neque porttitor elit, in imperdiet massa ante et quam. Nam condimentum scelerisque ex, non fermentum sem luctus vel. Nunc ultricies vehicula magna vitae pulvinar. Etiam aliquet quam ipsum, ut rutrum turpis tristique id. Donec feugiat leo eget risus porta rhoncus. Morbi fringilla lectus nec ultricies posuere. Aliquam eleifend, diam sit amet scelerisque mattis, nibh eros fringilla enim, vel aliquam nisl libero nec erat. Sed at iaculis metus. Fusce et turpis vel urna cursus pulvinar nec eu dolor. Curabitur ultricies, velit in sagittis consectetur, justo enim malesuada sem, at luctus tellus nisl quis massa. Curabitur dignissim, lectus id gravida facilisis, ex diam varius nibh, eu eleifend est ipsum vitae risus. Sed efficitur sed urna eget euismod. Phasellus non maximus libero, sit amet lobortis dolor. Morbi eu scelerisque odio, sit amet tempor tellus. Aliquam sit amet tellus facilisis, imperdiet felis id, posuere massa. Mauris dignissim, lectus id gravida tincidunt, lectus mauris gravida nunc, id posuere enim velit sit amet mi. Nam venenatis tellus eu facilisis consequat. Vivamus scelerisque odio vitae orci facilisis pellentesque. Proin tempor ornare ipsum et rutrum. Cras ac imperdiet dolor. Curabitur convallis dapibus magna sed rutrum. Sed pharetra ipsum id ante fringilla, nec porta neque aliquet. Integer erat ex, volutpat ut vestibulum eu, feugiat quis diam. Sed sit amet tellus leo. Maecenas aliquet, mauris eget viverra pellentesque, ipsum risus vestibulum lectus, ac porta ligula eros at risus. Aliquam erat volutpat.';

export function HeaderProblem() {
   const [showHeader, setShowHeader] = useState(true);
   return (
      <div
         className={css(Styles.container)}
         onScroll={event => {
            if (event.target.scrollTop > 50 && showHeader) {
               setShowHeader(false);
            }
            if (event.target.scrollTop < 50 && !showHeader) {
               setShowHeader(true);
            }
            console.log('scrolling', event.target.scrollTop);
         }}
      >
         <div
            className={`${css(Styles.headerContainer)} ${
               showHeader ? css(Styles.headerIn) : css(Styles.headerOut)
            }`}
         >
            <span>Sample Header Text</span>
         </div>
         <div
            className={`${css(Styles.bodyContainer)} ${
               showHeader ? css(Styles.bodyDown) : css(Styles.bodyUp)
            }`}
         >
            <span>{fakeText}</span>
         </div>
      </div>
   );
}

const Styles = StyleSheet.create({
   container: {
      height: 500,
      overflowY: 'auto',
      position: 'relative'
   },
   headerContainer: {
      position: 'absolute',
      backgroundColor: 'black',
      color: 'white',
      display: 'flex',
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
      left: 0,
      right: 0
   },
   headerOut: {
      transition: 'top 1s ease-in-out',
      top: -150
   },
   headerIn: {
      transition: 'top 1s ease-in-out',
      top: 0
   },
   bodyContainer: {
      backgroundColor: 'white',
      margin: '0 auto',
      padding: 10,
      width: 300,
      height: 800
   },
   bodyUp: {
      transition: 'margin-top 0.5s ease-in-out',
      marginTop: 0
   },
   bodyDown: {
      transition: 'margin-top 1s ease-in-out',
      marginTop: 50
   }
});
