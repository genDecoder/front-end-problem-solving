import { css, StyleSheet } from 'aphrodite';
import React from 'react';

const TEXTS = {
   buttonText: 'Continue',
   taskText:
      'Please center the text in the button. As reference see the png image in this repo. Please do not use calc. The size of the button and the font should to be dynamically adjusted based on the browser window size'
};
export function CssProblem() {
   return (
      <div className={css(Styles.texbox)} id="introduction">
         <p className={css(Styles.text)}>{TEXTS.taskText}</p>
         <form className={css(Styles.form)} method="POST">
            <input
               className={css(Styles.buttonLink)}
               id="buttonLink"
               type="button"
               value={TEXTS.buttonText}
            />
         </form>
      </div>
   );
}

const Styles = StyleSheet.create({
   textbox: {
      background: '#5ba68a',
      color: '#e6e4d8',
      height: '100vh',
      paddingTop: '2vw',
      paddingBottom: '2vw',
      paddingLeft: '5vw',
      paddingRight: '5vw'
   },
   text: {
      fontSize: '1em'
   },
   form: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: 20,
      marginBottom: 20
   },
   buttonLink: {
      background: '#d9c45b',
      border: 'none',
      borderRadius: '50%',
      color: '#e6e4d8',
      cursor: 'pointer',
      fontSize: '4vw',
      height: '25vw',
      lettersSpacing: '1px',
      outline: 'none',
      width: '25vw'
   }
});
