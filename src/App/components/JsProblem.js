import { css, StyleSheet } from 'aphrodite';
import React, { Fragment } from 'react';

const TEXTS = {
   title: 'JS',
   instruction: 'Provide answers to the two following questions:',
   template_strings_title: 'Template Strings',
   template_strings_instruction_1:
      'This is a es6 template string. What should be assigned to `x` such that the two conditions are `true`?',
   template_strings_instruction_2: 'let x = ???',
   template_strings_instruction_3: "`${x}` != '' + x // true",
   template_strings_instruction_4: "`${x}` !== '' + x // true",
   template_strings_solution: 'Neither "null" or "undefined"',
   sintax_title: 'Sintax',
   sintax_instruction_1:
      "const isAThing = typeof y === 'function' && 'something' || 'another things'",
   sintax_instruction_2:
      'Can you use a more straightforward construct to express similar semantics?',
   sintax_solution: "const isAThing = typeof y === 'function' ? 'something' : 'another things'",
   solution: 'Solution',
   and_or: 'And/Or'
};

export function JsProblem() {
   return (
      <Fragment>
         <div className={css(Styles.questionContainer)}>
            <span className={css(Styles.subTitleText)}>{TEXTS.template_strings_title}</span>
            <span className={css(Styles.paragraphText)}>
               {TEXTS.template_strings_instruction_1}
            </span>
            <span className={css(Styles.paragraphText)}>
               {TEXTS.template_strings_instruction_2}
            </span>
            <span className={css(Styles.paragraphText)}>
               {TEXTS.template_strings_instruction_3}
            </span>
            <span className={css(Styles.paragraphText)}>
               {TEXTS.template_strings_instruction_4}
            </span>
         </div>

         <div className={css(Styles.answerContainer)}>
            <span className={css(Styles.labelText)}>{TEXTS.solution}</span>
            <span className={css(Styles.quoteText)}>{TEXTS.template_strings_solution}</span>
         </div>

         <div className={css(Styles.questionContainer)}>
            <span className={css(Styles.subTitleText)}>{TEXTS.sintax_title}</span>
            <span className={css(Styles.paragraphText)}>{TEXTS.sintax_instruction_1}</span>
            <span className={css(Styles.paragraphText)}>{TEXTS.sintax_instruction_2}</span>
         </div>

         <div className={css(Styles.answerContainer)}>
            <span className={css(Styles.labelText)}>{TEXTS.solution}</span>
            <span className={css(Styles.quoteText)}>{TEXTS.sintax_solution}</span>
         </div>
      </Fragment>
   );
}

const Styles = StyleSheet.create({
   questionContainer: {
      backgroundColor: '#ebd8d557',
      display: 'flex',
      flexDirection: 'column',
      marginBottom: 10,
      padding: 10
   },
   answerContainer: {
      backgroundColor: '#d5ebdf57',
      display: 'flex',
      flexDirection: 'column',
      marginBottom: 10,
      padding: 10
   },
   titleText: {
      fontFamily: 'Open Sans',
      fontSize: '1em',
      fontWeight: 'bold',
      marginTop: 5
   },
   subTitleText: {
      fontFamily: 'Open Sans',
      fontSize: '0.8em',
      fontWeight: 'bold'
   },
   paragraphText: {
      fontFamily: 'Roboto',
      fontSize: '0.7em',
      marginLeft: 20,
      marginTop: 5
   },
   quoteText: {
      fontFamily: 'Roboto',
      fontSize: '0.7em',
      fontStyle: 'italic',
      marginLeft: 40,
      marginTop: 10
   },
   numberText: {
      fontFamily: 'Roboto',
      fontSize: '0.7em',
      fontWeight: 'bold',
      marginRight: 10
   },
   labelText: {
      fontFamily: 'Roboto',
      fontSize: '0.6em',
      fontWeight: 'bold',
      marginLeft: 30
   }
});
