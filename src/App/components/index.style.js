import { StyleSheet } from 'aphrodite';

export const Styles = StyleSheet.create({
   rowContainer: {
      display: 'flex',
      flexWrap: 'wrap',
      marginTop: 10,
      padding: 10
   },
   problemContainer: {
      boxSizing: 'border-box',
      display: 'flex',
      flexDirection: 'column',
      padding: 10,
      /* Colors */
      ':nth-child(1)': { backgroundColor: '#dec3c3' },
      ':nth-child(2)': { backgroundColor: '#e7d3d3' },
      ':nth-child(3)': { backgroundColor: '#f0e4e4' },
      ':nth-child(4)': { backgroundColor: '#f9f4f4' },
      /* Extra small devices (phones, 600px and down) */
      '@media only screen and (max-width: 600px)': {
         width: '100%'
      },
      /* Small devices (portrait tablets and large phones, 600px and up) */
      '@media only screen and (min-width: 600px) ': {
         width: '50%'
      },
      /* Medium devices (landscape tablets, 768px and up) */
      '@media only screen and (min-width: 768px) ': {
         width: '33.33%'
      },
      /* Extra large devices (large laptops and desktops, 1200px and up) */
      '@media only screen and (min-width: 1200px)': {
         width: '25%'
      }
   },
   titleText: {
      fontFamily: 'Open Sans',
      fontSize: '1.5em',
      fontWeight: 'bold'
   },
   paragraphText: {
      fontFamily: 'Roboto',
      fontSize: '0.8em',
      marginTop: 10
   },
   pathText: {
      backgroundColor: 'lightgray',
      borderRadius: 5,
      fontFamily: 'Open Sans',
      fontSize: '0.8em',
      fontWeight: 'bold',
      marginLeft: 5,
      marginRight: 5,
      padding: '3px 5px'
   },
   callToActionButton: {
      backgroundColor: 'red',
      borderRadius: 10,
      borderStyle: 'solid',
      borderWidth: 1
   }
});
