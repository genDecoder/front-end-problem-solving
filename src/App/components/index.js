import { css } from 'aphrodite';
import React, { useState } from 'react';
import { ActionButton } from '../common/components/ActionButton';
import { SimpleModal } from '../common/components/SimpleModal';
import { Styles } from './index.style';
import { CssProblem } from './CssProblem';
import { DuplicateEntriesProblem } from './DuplicateEntriesProblem';
import { HeaderProblem } from './HeaderProblem';
import { JsProblem } from './JsProblem';
// import { Route } from 'react-router-dom';
// import * as routes from '../common/appRoutes';
// import Header from './Header'

const NO_DEMO_PROBLEMS = ['coin', 'fileParsing', 'filtering', 'jsAsync'];
const WITH_DEMO_PROBLEMS = ['css', 'duplicatesEntries', 'header', 'js'];

const MODAL_CONTENT_MAP = {
   [WITH_DEMO_PROBLEMS[0]]: <CssProblem />,
   [WITH_DEMO_PROBLEMS[1]]: <DuplicateEntriesProblem />,
   [WITH_DEMO_PROBLEMS[2]]: <HeaderProblem />,
   [WITH_DEMO_PROBLEMS[3]]: <JsProblem />
};

const TEXTS = {
   [NO_DEMO_PROBLEMS[0]]: {
      title: 'Coin Problem',
      solutionPath: 'src/App/problems/coin.js',
      testPath: 'src/App/problems/__tests__/coin.test.js'
   },
   [NO_DEMO_PROBLEMS[1]]: {
      title: 'File Parsing Problem',
      solutionPath: 'src/App/problems/file-parsing.js',
      testPath: 'src/App/problems/__tests__/file-parsing.test.js'
   },
   [NO_DEMO_PROBLEMS[2]]: {
      title: 'Filtering Problem',
      solutionPath: 'src/App/problems/filtering.js',
      testPath: 'src/App/problems/__tests__/filtering.test.js'
   },
   [NO_DEMO_PROBLEMS[3]]: {
      title: 'JS Async Problem',
      solutionPath: 'src/App/problems/js-async.js',
      testPath: 'src/App/problems/__tests__/js-async.test.js'
   },
   [WITH_DEMO_PROBLEMS[0]]: {
      title: 'CSS'
   },
   [WITH_DEMO_PROBLEMS[1]]: {
      title: 'Duplicate Entries'
   },
   [WITH_DEMO_PROBLEMS[2]]: {
      title: 'Header'
   },
   [WITH_DEMO_PROBLEMS[3]]: {
      title: 'JS'
   },
   labels: {
      solution_is_in: 'Solution:',
      test_can_be_found_in: 'Test:'
   }
};

function App() {
   const [modal, setModal] = useState(null);
   return (
      <div>
         <div className={css(Styles.rowContainer)}>
            {NO_DEMO_PROBLEMS.map(problem => (
               <div className={css(Styles.problemContainer)} key={problem}>
                  <span className={css(Styles.titleText)}>{TEXTS[problem].title}</span>
                  <span className={css(Styles.paragraphText)}>
                     {TEXTS.labels.solution_is_in}
                     <span className={css(Styles.pathText)}>{TEXTS[problem].solutionPath}</span>
                  </span>
                  <span className={css(Styles.paragraphText)}>
                     {TEXTS.labels.test_can_be_found_in}
                     <span className={css(Styles.pathText)}>{TEXTS[problem].testPath}</span>
                  </span>
               </div>
            ))}
         </div>

         <div className={css(Styles.rowContainer)}>
            {WITH_DEMO_PROBLEMS.map(problem => (
               <div className={css(Styles.problemContainer)} key={problem}>
                  <span className={css(Styles.titleText)}>{TEXTS[problem].title}</span>
                  <ActionButton onClick={() => setModal(MODAL_CONTENT_MAP[problem])} text="See" />
               </div>
            ))}
         </div>

         {modal && <SimpleModal close={() => setModal(null)}>{modal}</SimpleModal>}
      </div>
   );
}

export default App;
