import { css, StyleSheet } from 'aphrodite';
import Proptypes from 'prop-types';
import React from 'react';

export function ActionButton({ onClick, text }) {
   return (
      <div className={css(Styles.container)} onClick={onClick} role="presentation">
         <span className={css(Styles.text)}>{text}</span>
      </div>
   );
}

ActionButton.propTypes = {
   onClick: Proptypes.func.isRequired,
   text: Proptypes.string.isRequired
};

const Styles = StyleSheet.create({
   container: {
      alignSelf: 'flex-end',
      backgroundColor: '#ff6f69',
      borderRadius: 10,
      cursor: 'pointer',
      padding: '5px 7px',
      textAlign: 'center',
      width: 'fit-content'
   },
   text: {
      color: 'white',
      fontFamily: 'Roboto',
      fontSize: '1em'
   }
});
