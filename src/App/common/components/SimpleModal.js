import { css, StyleSheet } from 'aphrodite';
import Proptypes from 'prop-types';
import React, { Fragment } from 'react';

export function SimpleModal({ children, close }) {
   function closeBackdrop(e) {
      e.stopPropagation();
      close();
   }
   return (
      <Fragment>
         <div
            className={css(Styles.backdropContainer)}
            onClick={closeBackdrop}
            role="presentation"
         />
         <div className={css(Styles.modalContainer)}>
            {children}
            <div className={css(Styles.buttonContainer)} onClick={close} role="presentation">
               <span className={css(Styles.buttonText)}>Close</span>
            </div>
         </div>
      </Fragment>
   );
}

SimpleModal.propTypes = {
   children: Proptypes.any.isRequired,
   close: Proptypes.func.isRequired
};

const Styles = StyleSheet.create({
   backdropContainer: {
      backgroundColor: '#00000078',
      justifyContent: 'center',
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
   },
   modalContainer: {
      position: 'absolute',
      backgroundColor: 'white',
      borderRadius: 10,
      borderStyle: 'solid',
      borderWidth: 1,
      padding: 10,
      width: '80%',
      '@media only screen and (max-width: 600px)': {
         width: '90%'
      },
      // centering
      left: '50%',
      top: '50%',
      transform: 'translateX(-50%) translateY(-50%)'
   },
   buttonContainer: {
      borderStyle: 'solid',
      borderWidth: 1,
      cursor: 'pointer',
      float: 'right',
      padding: '4px 5px',
      width: 'fit-content'
   },
   buttonText: {
      fontSize: '1em',
      fontWeight: 'bold'
   }
});
